nxVersion = "0.101";
nxKickLimit = "2";
nxWelcomeTimeLimit = "120000";
var leaveLimit = 3600;
var autoAdd = 900;
var limitList = "0:480;20:420;30:360";
var leftUserList = JSON.parse(localStorage.getItem('leftUsers')) ? JSON.parse(localStorage.getItem('leftUsers')) : { length: 0, reset: Date.now() };
var lockdown = 0;
var queueEnabled = 0;
var queueList = [];
var bot = {
    OnChat: function (data) {
        bot.resetAFK(1, data);
        if (!bot.isRanked(data.uid, 2)) {
            bot.CheckSpam(data);
        }
        if (lockdown != 0 && bot.isRanked(data.uid, lockdown) == 0) {
            return 0;
        }
        if (data.message.indexOf("!") !== 0) {
            return 0;
        }
        if (data.message.indexOf("!lockdown") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                if (data.message.length > 10) {
                    if (data.message.indexOf("resident") > -1) {
                        lockdown = 1;
                        API.sendChat("Lockdown is now on Resident DJ and above");
                    } else if (data.message.indexOf("mod") > -1) {
                        lockdown = 2;
                        API.sendChat("Lockdown is now on Moderators and above");
                    } else if (data.message.indexOf("off") > -1) {
                        lockdown = 0;
                        API.sendChat("Lockdown is now OFF");
                    }
                } else {
                    if (lockdown == 0) {
                        API.sendChat("Lockdown is OFF");
                    } else if (lockdown == 1) {
                        API.sendChat("Lockdown is on Resident DJ and above");
                    } else if (lockdown == 2) {
                        API.sendChat("Lockdown is on Moderator and above");
                    }
                }
            }
        } else if (data.message.indexOf("!welcome") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                if (data.message.length > 9) {
                    if (data.message.indexOf("on") > -1) {
                        JoinMsg = 1;
                        API.sendChat("Welcome message is now ON");
                    } else if (data.message.indexOf("off") > -1) {
                        JoinMsg = 0;
                        API.sendChat("Welcome message is now OFF");
                    }
                } else if (JoinMsg == 0) {
                    API.sendChat("Current welcome message status is OFF");
                } else {
                    API.sendChat("Current welcome message status is ON");
                }
            }
        } else if (data.message.indexOf("!bot") == 0) {
            API.sendChat("@" + data.un + " Hey i'm a bot coded by Enyxx, with some tweaks by gigosaurus and TheOne :D v" + nxVersion + ".");
        } else if (data.message.indexOf("!ver") == 0) {
            API.sendChat("v" + nxVersion + ".");
        } else if (data.message.indexOf("!limit") == 0) {
            if (data.message.length > 7 && bot.isRanked(data.uid, 2)) {
                if (data.message.indexOf("add") > -1) {
                    bot.addLimit(data.message);
                } else if (data.message.indexOf("remove") > -1) {
                    bot.removeLimit(data.message);
                } else if (data.message.indexOf("clear") > -1) {
                    limitList = "";
                } else if (data.message.indexOf("reset") > -1) {
                    limitList = "0:480;20:420;30:360";
                }
            }
            bot.SayLimits();
        } else if (data.message.indexOf("!allow") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Allow(data);
            }
        } else if (data.message.indexOf("!disallow") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Disallow(data);
            }
        } else if (data.message.indexOf("!group") == 0) {
            API.sendChat("Join our facebook group to stay updated on room events and music news: https://www.facebook.com/groups/turntablerockstars/");
        }
        else if (data.message.indexOf("!theme") == 0) {
            if (data.message.length > 7 && bot.isRanked(data.uid, 2)) {
                temptheme = data.message.substr(7);
                theme = (temptheme == "off" ? "" : temptheme);
            }
            bot.SayTheme();
        } else if (data.message.indexOf("!crash") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                destroy();
            }
        } else if (data.message.indexOf("!alien") == 0) {
            if (data.uid == "3426664" || data.uid == "4132969") {
                API.sendChat("/me My lord, we are ready to destroy humanity");
            } else {
                API.sendChat("/me pew pew pew! die humans")
            }
        } else if (data.message.indexOf("!add") == 0 || data.message.indexOf("!join") == 0) {
            bot.Add(data);
        } else if (data.message.indexOf("!move") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Move(data.message);
            }
        } else if (data.message.indexOf("!remove") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Remove(data.message);
            }
        } else if (data.message.indexOf("!kick") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Kick(data.message);
            }
        } else if (data.message.indexOf("!ban") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                bot.Ban(data.message);
            }
        } else if (data.message.indexOf("!dc") == 0 || data.message.indexOf("!checkpos") == 0) {
            bot.CheckPos(data);
        } else if (data.message.indexOf("!reloadwaitlist") == 0 || data.message.indexOf("!refreshwaitlist") == 0 || data.message.indexOf("!fixwaitlist") == 0) {
            if (bot.isRanked(data.uid, 2)) {
                for (var i in API.getWaitList()) {
                    API.moderateMoveDJ(API.getWaitList()[i].username, API.getWaitListPosition(API.getWaitList()[i].id));
                }
            }
        } else if (data.message.indexOf("!genre") == 0) {
            if (bot.isRanked(data.uid, 1)) {
                if (data.message.length > 7) {
                    bot.Genre(data.message.substr(7).replace(/ /g, '_'), '', data.message.substr(7), 0);
                } else {
                    bot.Genre(API.getMedia().author.replace(/ /g, '_'), '', API.getMedia().author, 0);
                }
            }
        } else if (data.message.indexOf("!help") == 0 || data.message.indexOf("!info") == 0 || data.message.indexOf("!rules") == 0) {
            API.sendChat("Rock/Metal Genres Only! Chat in English Only! Song length limits dependent on number in Wait List. AFK DJs will be removed from Wait List! Click on room info or visit the following link for Room Rules & Bot Commands http://goo.gl/5eSVxC");
        } else if (data.message.indexOf("!respect") === 0) {
            API.sendChat("If you don't like a song, mute, meh and move on. Respect your fellow DJs and don't complain about their song choices.");
        } else {
            answer = bot.SocialAnswer(data);
            if (answer != 0) {
                bot.Woot();
                API.sendChat(answer);
            }
        }
    },
    SocialAnswer: function (data) {
        var msgs = [];
        if (data.message.indexOf("!life") === 0) {
            msgs = ["No you're not hardcore! Unless you live hardcore! And the legend of the rent was way hardcore!",
                "Life is like a box of chocolates, you never know what you're gonna get.",
                "I don\u2019t know this life of which you speak."];
        } else if (data.message.indexOf("!hug") === 0) {
            msgs = ["I don't do hugs,unless they're naked hugs!",
                "Go and hug a tree you hippy!",
                "Can I hug your throat with my hands?",
                "You may hug me, but don't ask what that hard thing sticking into your hip is!",
                "Heather does the hugging around here!",
                "I would give you a hug, but I need a kiss to recharge my battery!",
                "I would give you a hug, but I've reached my limit for the day.",
                "Hug? Not such a safe thing to do in the middle of a mosh pit.",
                "Just get it over with...",
                "Hugs may lead to harder things.",
                "Sure I\u2019ll hug you, but I won\u2019t be your sugar daddy.",
                "A hug is a currency you can\u2019t cash.",
                "You better buy me a drink first and none of that hard to reach stuff either."];
        } else if (data.message.indexOf("!stop") === 0) {
            msgs = ["I'll stop as soon as you leave.  I promise.",
                "Stop? What do you take me for, a traffic light?"];
        } else if (data.message.indexOf("!smile") === 0) {
            msgs = ["Smiling hurts me.",
                "If you knew what I know, you wouldn't be smiling either.",
                "I'll smile at anything unless you play Lady Gaga.",
                "I am smiling, can\u2019t you tell?"];
        } else if (data.message.indexOf("!will") === 0) {
            msgs = ["Ask me no more questions, I'll tell you no more lies.",
                "That's a personal question.",
                "I think not.",
                "I could, but that sounds awfully boring.",
                "Tried that once, I\u2019d rather not.",
                "Ask my 8ball and see what it says."];
        } else if (data.message.indexOf("!answer") === 0) {
            msgs = ["The Answer?  Try wikipedia.",
                "Ask me a question and maybe I'll answer!",
                "I don't think you'd like my answer.",
                "Ask my 8ball and see what it says."];
        } else if (data.message.indexOf("!8ball") === 0) {
            msgs = ["It is certain.",
                "It is decidedly so.",
                "Without a doubt.",
                "I'm too drunk to care.",
                "You may rely on it.",
                "As I see it, yes.",
                "Most likely.",
                "Outlook good.",
                "Signs point to yes.",
                "Yes.",
                "Reply hazy, try again.",
                "Ask again later.",
                "Better not tell you now.",
                "Cannot predict now.",
                "Concentrate and ask again.",
                "Don't count on it.",
                "My reply is no.",
                "My sources say no.",
                "Outlook not so good.",
                "Very doubtful.",
                "You're screwed"];
        } else if (data.message.indexOf("!sing") === 0) {
            msgs = ["Trust me, you don\u2019t want to hear my singing voice. I don\u2019t want to make everyone jealous.",
                "We roll tonight to the guitar bite, and for those about to rock, I salute you. :metal: ",
                "No you're not hardcore! Unless you live hardcore! And the legend of the rent was way hardcore!"];
        } else if (data.message.indexOf("!die") === 0) {
            msgs = ["Oh thank you very much. That's you off my Christmas card list!",
                "I'm working on it, how about you show me how it's done?",
                "My my hey hey rock \u2018n\u2019 roll will never die"];
        } else if (data.message.indexOf("!time") === 0) {
            msgs = ["Get yourself a freaking watch..you cheapo!",
                "By the looks of the sun . . . Oh god! I can\u2019t see now",
                "It\u2019s time to freakin\u2019 ROCK! :metal:",
                "I\u2019m not your timekeeper!! Ask mommie dearest",
                "Do I look like a clock to you?"];
        } else if (data.message.indexOf("!go") === 0) {
            msgs = ["I'm not going anywhere, Why would I want to leave this awesome room?",
                "And leave you and all your awesomeness?",
                "Rock n rollers don\u2019t go, they just fade away....",
                "~sobs~ parting is such sweet sorrow"];
        } else if (data.message.indexOf("!cookie") === 0) {
            msgs = ["Um nom nom , C is for Cookie {::}! ",
                "I think I found one for you under the DJ stage.....",
                "Cookie little boy?",
                "Do you know....the Muffin Man?"];
        } else if (data.message.indexOf("!hello") === 0) {
            msgs = ["Thanks for saying hello, but stop playing about and enjoy the music!",
                "Put your horns up for @" + data.un + " :metal:",
                "Wassup @" + data.un + ", we have confiscated your queue. Do not attempt to adjust your sound, picture, or playlist. We will make you play awesomeness!"];
        } else if (data.message.indexOf("!party") === 0) {
            msgs = ["Let\u2019s burn this mother down! ...with Rock and Metal.",
                "The last time I had this many people organized around me they tried to stop me from drinking.",
                "It\u2019s about time we celebrate my quinceanera."];
        } else if (data.message.indexOf("!bye") === 0) {
            msgs = ["G.T.F.O.!",
                "This time make it permanent.",
                "I don\u2019t want to hear you say \u201cGoodbye\u201d again because I don\u2019t want to see you again.",
                "Lots of people say \u201cSee you later\u201d you don\u2019t say it often enough.",
                "Use this time to get some better songs; I\u2019ll spend mine trying to forget your face."];
        } else if (data.message.indexOf("!kill") === 0) {
            return "So, when do we do this? Tomorrow? The day after tomorrow?";
        } else if (data.message.indexOf("!woot") === 0) {
            return "I suppose this song doesn't make me want to punch somebody...or maybe it does?";
        } else if (data.message.indexOf("!w00t") === 0) {
            return "Ooohh I love this song...Oh no, wrong one...this one sucks!";
        } else if (data.message.indexOf("!<span class=\"emoji-glow\"><span class=\"emoji emoji-1f3b8\"></span></span>") === 0) {
            return "Ok, I'll stop with the air guitar!";
        } else if (data.message.indexOf("!headbang") === 0) {
            return "Give me some WD-40 and I'll headbang all night";
        } else if (data.message.indexOf("!groovy") === 0) {
            return "Yeaaaaaah babyyyy yeaaaaah!";
        } else if (data.message.indexOf("!dance") === 0) {
            return "I'll dance, but only because I need to go to the bathroom.";
        } else if (data.message.indexOf("!bop") === 0) {
            return "I'll bop my head if it will make you happy. But I have a freaking headache.";
        } else if (data.message.indexOf("!awesome") === 0) {
            return "I love it when you tell me what to do...NOT!";
        } else if (data.message.indexOf("!poledance") === 0) {
            return "Go hit woot yourself!  Grrrrrr";
        } else if (data.message.indexOf("!striptease") === 0) {
            return "Not even a drunken Rockbot5000 would awesome this song.  But I would!";
        } else if (data.message.indexOf("!rock") === 0) {
            return "Now raise your goblet of rock. It's a toast to those who rock!";
        } else if (data.message.indexOf("!<span class=\"emoji-glow\"><span class=\"emoji emoji-metal\"></span></span>") === 0) {
            return "Dude, is my face okay? I think you melted it off!";
        } else if (data.message.indexOf("!sway") === 0) {
            return "I\u2019ll dance if you promise not to play those DUB songs in your queue";
        } else if (data.message.indexOf("!mosh") === 0) {
            return "To this song? You're kidding?";
        } else if (data.message.indexOf("!jam") === 0) {
            return "Do I HAVE to awesome this song?";
        } else if (data.message.indexOf("!doabarrelroll") === 0) {
            return "Give me a little bourbon and I'll bop all night";
        } else if (data.message.indexOf("!boogie") === 0) {
            return "Give me some drinks and smokes and I'll dance all night";
        }
        if (msgs.length > 0) {
            var i = Math.floor(Math.random() * msgs.length);
            return msgs[i];
        }
        return 0;
    },
    OnJoin: function (data) {
        console.log("Join" + data);
        bot.addAFK(data);
        if (JoinMsg == 1 && bot.getAFK(data.id).wtime == 0 && API.getWaitListPosition(data.id) == -1) {
            API.sendChat("Welcome to the House of Rock & Metal, @" + data.username);
            bot.setWelcome(data.id, 1);
            setTimeout(function () {
                bot.setWelcome(data.id, 0)
            }, nxWelcomeTimeLimit);
        }
    },
    OnLeave: function (data) {
        if (bot.getAFK(data.id).lastWaitList > -1) {
            if (!leftUserList[data.username.toLowerCase()]) leftUserList.length++;
            leftUserList[data.username.toLowerCase()] = { 'pos': bot.getAFK(data.id).lastWaitList, 'time': Date.now(), 'id': data.id };
        }
        bot.removeAFK(data);
        if (((Date.now() - leftUserList.reset) / 1000) > leaveLimit) {
            for (var key in leftUserList) {
                if (((Date.now() - leftUserList[key].time) / 1000) > leaveLimit) {
                    console.log('Deleting old data for: ' + key + ' ' + bot.toPrettyTime((Date.now() - leftUserList[key].time) / 1000));
                    delete leftUserList[key];
                    leftUserList.length--;
                }
            }
            leftUserList.reset = Date.now();
        }
        localStorage.setItem('leftUsers', JSON.stringify(leftUserList));
    },
    CheckPos: function (data) {
        var parts = data.message.replace('!checkpos', '').replace('!dc', '').trim();
        var name = parts.replace('@', '');
        if(name) {
            if (!bot.isRanked(data.uid, 2) && name.toLowerCase() != data.un.toLowerCase()) {
                return;
            }
        } else {
            name = data.un.toLowerCase();
        }
        var user = leftUserList[name.toLowerCase()];
        if (user) {
            API.sendChat('/me @' + name + ' left position ' + (user.pos + 1) + ' about ' + bot.toPrettyTime((Date.now() - user.time) / 1000) + ' ago');
            if (((Date.now() - user.time) / 1000) <= autoAdd) {
                bot.AddWaitList(user.id);
                setTimeout(function () {
                    bot.MoveWaitList(user.id, (user.pos + 1));
                    var timesChecked = 0;
                    var moveCheck = setInterval(function() {
                        // Done as an interval in case there is any lag. Without it, the API often takes too long.
                        timesChecked++;
                        if( API.getWaitList()[user.pos].id === user.id ) {
                            console.log('Automatically added @' + name + ' back in line');
                            delete leftUserList[name];
                            clearInterval(moveCheck);
                        }
                        if(timesChecked === 5) {
                            API.sendChat( '@' + name + ' could not be moved. Try reconnecting and !dc again.');
                            clearInterval(moveCheck);
                        }
                    }, 1000);
                }, 1000);
            }
        } else {
            API.sendChat('/me @' + name + ' not found in list of disconnected users');
        }
    },
    OnCurate: function (data) {
        bot.resetAFK(2, data.user);
    },
    OnAdvance: function (data) {
        bot.AutoSkip();
        if (data == null) {
            return;
        }
        bot.CheckHistory();
        if (limitList == null) {
            for (i = 0; i < AFKbotList.length; i++) {
                if (AFKbotList[i].id == data.dj.id) {
                    if (AFKbotList[i].Allowed == 1) AFKbotList[i].Allowed = 0
                }
            }
            return;
        }
        var dur = data.media.duration;
        var list = API.getWaitList().length;
        var limits = limitList.split(";");
        for(i = 0; i < limits.length; i++) {
            if (limits[i] != "" && list >= limits[i].split(":")[0] && dur > limits[i].split(":")[1]) {
                bot.SkipLimit(data.dj);
                for (i = 0; i < AFKbotList.length; i++) {
                    if (AFKbotList[i].id == data.dj.id) {
                        if (AFKbotList[i].Allowed == 1) {
                            AFKbotList[i].Allowed = 0
                        }
                    }
                }
                return;
            }
        }
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == data.dj.id) {
                if (AFKbotList[i].Allowed == 1) {
                    AFKbotList[i].Allowed = 0
                }
            }
        }
    },
    SkipLimit: function (dj) {
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == dj.id) {
                if (AFKbotList[i].Allowed == 0) {
                    API.sendChat("@" + dj.username + " this song is over the limit and has to be skipped.");
                    API.moderateForceSkip();
                }
            }
        }
    },
    CheckHistory: function () {
        $.each(API.getHistory(), function (index, value) {
            if (value.media.id == API.getMedia().id) {
                if (index != 0 || API.getDJ.id != value.user.id) {
                    for (i = 0; i < AFKbotList.length; i++) {
                        if (AFKbotList[i].id == API.getDJ().id) {
                            if (AFKbotList[i].Allowed == 0) {
                                API.sendChat("@" + API.getDJ().username + " this song has been played recently and so it will be skipped.");
                                API.moderateForceSkip();
                            }
                            return;
                        }
                    }
                }
            }
        })
    },
    Allow: function (data) {
        if (data.message.split(" ").length < 2) {
            return;
        }
        var user = data.message.replace("@", "").split(" ")[1];
        if (user == "" || user == null) {
            return;
        }
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == bot.getUserID(user)) {
                if (AFKbotList[i].Allowed == 1) {
                    API.sendChat("Next song by @" + user + " is already allowed.");
                } else {
                    AFKbotList[i].Allowed = 1;
                    API.sendChat("Allowing the next song by @" + user + " to play regardless.");
                }
            }
        }
    },
    Disallow: function (data) {
        if (data.message.split(" ").length < 2) {
            return;
        }
        var user = data.message.replace("@", "").split(" ")[1];
        if (user == "" || user == null) return;
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == bot.getUserID(user)) {
                if (AFKbotList[i].Allowed == 0) {
                    API.sendChat("Next play by @" + user + " is not already bypassing any restrictions.");
                } else {
                    AFKbotList[i].Allowed = 0;
                    API.sendChat("Next play by @" + user + " will no longer bypass any restrictions.");
                }
            }
        }
    },
    OnVoteUpdate: function (data) { },
    OnListUpdate: function (data) {
        bot.updateListRecords(data);
        var user = data[4];
        var afk = bot.getAFK(user.id);
        console.log("AFKCheck - " + user.username + ": " + afk.time + "/2700000");
        if (afk.time > 2700000 && afk.iKick != 1 && afk.iWarn != 1 && !bot.DCCheck()) {
            API.sendChat("@" + user.username + " you are showing 45+ minutes AFK, please respond or be removed from the waitlist.");
            AFKbotList[afk.pos].iWarn = 1;
            setTimeout(function () {
                console.log("time's up : " + user.username);
                var afk = bot.getAFK(user.id);
                if (afk.time > 2700000 && afk.iKick != 1) {
                    AFKbotList[afk.pos].iKick = 1;
                    API.moderateRemoveDJ(user.id);
                    AFKbotList[afk.pos].iWarn = 0;
                    AFKbotList[afk.pos].iKick = 0;
                    AFKbotList[afk.pos].Kicks++;
                    AFKbotList[afk.pos].lastWaitList = -1;
                    if (AFKbotList[afk.pos].Kicks >= nxKickLimit) {
                        AFKbotList[afk.pos].Kicks = 0;
                        API.moderateBanUser(user.id, 1, API.BAN.HOUR);
                        API.sendChat("@" + user.username + ", you are being kicked for being AFK in the waitlist twice in a row");
                    } else {
                        API.sendChat("@" + user.username + ", you are being removed from the waitlist for being AFK");
                    }
                }
            }, 300 * 1000);
        }

        if (data.length == 50 && queueEnabled == 0) {
            queueEnabled = 1;
            API.moderateLockWaitList(true, false);
            API.sendChat("Waitlist full. Type !join to be added to the queue for the waitlist.");
        } else if (data.length <= 47 && queueEnabled == 1) {
            API.moderateLockWaitList(false, false);
            queueEnabled = 0;
        }
        if (queueEnabled == 1 && queueList.length > 0 && data.length < 50) {
            while (API.getUser(queueList[0]).id == null && API.getWaitListPosition(queueList[0]) == -1 && queueList.length > 0) {
                queueList.shift();
            }
            bot.AddWaitList(queueList[0]);
            queueList.shift();
            console.log(queueList);
        }
    },
    OnCommand: function (cmd) {
        if (cmd.indexOf("/botAFK") == 0) {
            user = cmd.substr(8);
            console.log(user + " - " + bot.getAFK(user).time);
            time = bot.convertMS(bot.getAFK(user).time);
            $("#chat-messages").append('<div class="message welcome nxnotif"><span class="text">AFK time: ' + user + " - " + time.h + ":" + time.m + ":" + time.s + "</span></div>");
        }
    },
    DCCheck: function () {
        return ($("div .disconnect").length ? ($("#chat-messages").append('<div class="message welcome nxnotif"><span class="text">BOT LOST CONNECTION !</span></div>'), 1) : 0);
    },
    isRanked: function (user, perm) {
        return (API.getUser(user).role >= perm ? 1 : 0);
    },
    SayTheme: function () {
        if (theme != "") {
            API.sendChat("The current theme is " + theme + ", ask a Mod for details.");
        } else {
            API.sendChat("There is no theme at the moment.");
        }
    },
    AutoSkip: function () {
        timer = API.getTimeRemaining();
        // console.log(timer);
        if (timer < 0) {
            console.log("Skip");
            API.moderateForceSkip();
        } else {
            setTimeout(function () { bot.AutoSkip() }, 1000 * timer + 5000);
        }
    },
    ClicButton: function (data) { },
    Woot: function () {
        console.log("Wooted");
        $("div#woot").delay(3800).trigger("click")
    },
    CheckSpam: function (data) {
        if (data.type == "message") {
            for (i = 0; i < AFKbotList.length; i++) {
                if (data.uid == AFKbotList[i].id) {
                    if (AFKbotList[i].LastMessage == data.message && AFKbotList[i].LastMessageTime > Date.now() - 60000) {
                        AFKbotList[i].Repeats = AFKbotList[i].Repeats + 1;
                        if (AFKbotList[i].Repeats == 3) {
                            API.sendChat("@" + data.un + " if you continue to spam you will be banned.");
                        } else if (AFKbotList[i].Repeats == 5) {
                            API.moderateBanUser(AFKbotList[i].id, 1, API.BAN.DAY);
                        }
                    } else {
                        AFKbotList[i].LastMessage = data.message;
                        AFKbotList[i].Repeats = 0;
                    }
                    return;
                }
            }
        }
    },
    initAFK: function () {
        window.AFKbotList = [];
        $.each(API.getUsers(), function (data, user) {
            AFKbotList.push({
                id: user.id,
                WTime: 0,
                Stime: new Date,
                iKick: "0",
                iWarn: "0",
                Kicks: "0",
                Allowed: "0",
                LastMessage: "",
                LastMessageTime: "0",
                Repeats: "0",
                lastWaitList: "-1"
            })
        })
    },
    addAFK: function (user) {
        for (i = 0; i < AFKbotList.length; i++) {
            if (user.id == AFKbotList[i].id) {
                return;
            }
        }
        AFKbotList.push({
            id: user.id,
            WTime: 0,
            Stime: new Date,
            iKick: "0",
            iWarn: "0",
            Kicks: "0",
            Allowed: "0",
            LastMessage: "",
            LastMessageTime: "0",
            Repeats: "0",
            lastWaitList: "-1"
        })
    },
    removeAFK: function (user) { },
    resetAFK: function (type, data) {
        if (type != 1 || data.type == "message") {
            for (i = 0; i < AFKbotList.length; i++) {
                if (data.uid == AFKbotList[i].id) {
                    AFKbotList[i].Stime = new Date;
                    return;
                }
            }
        }
    },
    updateListRecords: function (data) {
        for (var i = 0; i < AFKbotList.length; i++) {
            AFKbotList[i].lastWaitList = -1;
            for (var j = 0; j < data.length; j++) {
                if (data[j].id == AFKbotList[i].id) {
                    AFKbotList[i].lastWaitList = j;
                }
            }
        }
    },
    getAFK: function (id) {
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == id) return {
                time: new Date - AFKbotList[i].Stime,
                wtime: AFKbotList[i].WTime,
                iWarn: AFKbotList[i].iWarn,
                iKick: AFKbotList[i].iKick,
                pos: i,
                lastWaitList: AFKbotList[i].lastWaitList
            }
        }
    },
    setWelcome: function (id, value) {
        for (i = 0; i < AFKbotList.length; i++) {
            if (AFKbotList[i].id == id) {
                AFKbotList[i].WTime = value;
            }
        }
    },
    convertMS: function (ms) {
        var day, hr, min, sec;
        sec = Math.floor(ms / 1000);
        min = Math.floor(sec / 60);
        hr = Math.floor(min / 60);
        day = Math.floor(hr / 24);
        return {
            d: day,
            h: hr % 24,
            m: (min % 60 < 10 && hr > 0 ? "0" + min % 60 : min % 60),
            s: (sec % 60 < 10 ? "0" + sec % 60 : sec % 60)
        }
    },
    toPrettyTime: function (secs) {
        var sec_num = parseInt(secs, 10);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) hours = "" + hours;
        if (minutes < 10) minutes = "" + minutes;
        if (seconds < 10) seconds = "" + seconds;

        var time = (hours !== '0' ? (hours + ' hours, ') : '') + (minutes !== '0' ? minutes + ' minutes and ' : '') + seconds + ' seconds';
        return time;
    },
    getUserID: function (user) {
        var id = null;
        $.each(API.getUsers(), function (a, data) {
            if (data.username == user) {
                return id = data.id, !1
            }
        });
        return id
    },
    addLimit: function (msg) {
        var params = msg.split(" ");
        if (params.length != 4) return;
        var limits = limitList.split(";");
        if (limits.length == 1) {
            if (limits[0] == "") {
                limitList = params[2] + ":" + params[3];
            } else if (limits[0].split(":")[0] < params[2]) {
                limitList += ";" + params[2] + ":" + params[3];
            } else if (limits[0].split(":")[0] > params[2]) {
                limitList = params[2] + ":" + params[3] + ";" + limitList;
            } else {
                limitList = params[2] + ":" + params[3];
            }
            return;
        }
        for (i = 0; i < limits.length - 1; i++) {
            if (i == 0 && limits[i].split(":")[0] > params[2]) {
                limitList = params[2] + ":" + params[3] + ";" + limitList
                return;
            }
            if (limits[i] != "" && limits[i+1] != "") {
                if (limits[i].split(":")[0] < params[2] && limits[i + 1].split(":")[0] > params[2]) {
                    limitList = "";
                    for (ii = 0; ii < limits.length; ii++) {
                        if (ii < i) {
                            limitList = limits[ii] + ";";
                        } else if (ii == i + 1) {
                            limitList = params[2] + ":" + params[3] + ";" + limits[ii];
                        } else if (ii > i) {
                            limitList = limits[ii] + ";";
                        }
                    }
                    limitList = limitList.substr(0, limitList.lastIndexOf(";"));
                    return;
                }
            }
        }
        if (limits[limits.length-1].split(":")[0] < params[2]) {
            limitList += ";" + params[2] + ":" + params[3];
        }
    },
    removeLimit: function (msg) {
        var params = msg.split(" ");
        if (params.length != 3) return;
        var limits = limitList.split(";");
        for(i = 0; i < limits.length; i++) {
            if(limits[i].split(":")[0] == params[2]) {
                delete limits[i];
            }
        }
        limitList = "";
        for(i = 0; i < limits.length; i++) {
            if(limits[i] != null && limits[i] != "" && limits[i] != undefined) {
                limitList += limits[i] + ";";
            }
        }
        limitList = limitList.substr(0, limitList.lastIndexOf(";"));
    },
    SayLimits: function () {
        var msg = "";
        var limits = limitList.split(";");
        for(i = 0; i < limits.length; i++) {
            if (limits[i] != "") {
                var time = bot.convertMS(limits[i].split(":")[1] * 1000);
                time = time.m + ":" + time.s;
                if (limits[i].split(":")[0] == "0") {
                    if (limits.length > 1) {
                        msg += (limits[i + 1].split(":")[0]-1) + " and under is " + time + ", "
                    } else {
                        API.sendChat("Song length limit is " + time);
                        return;
                    }
                } else {
                    if (i == limits.length - 1 && i != 0) {
                        msg = msg.substr(0, msg.lastIndexOf(',')) +  " and ";
                    }
                    msg += limits[i].split(":")[0] + "+ is " + time + ", ";
                }
            }
        }
        if (msg == "") {
            API.sendChat("There is not currently any song length limit.");
        } else {
            API.sendChat("Song length limit is based on number in wait list. " + msg.substr(0, msg.lastIndexOf(', ')));
        }
    },
    AddWaitList: function (id) {
        if(API.getWaitListPosition(id) > -1) {
            return;
        }
        if (API.getWaitList().length < 50) {
            API.moderateAddDJ(id);
        } else {
            queueList.push(id);
            API.sendChat("@" + API.getUser(id).username + " you have been added to the queue for the waitlist.");
            console.log(queueList);
        }
    },
    MoveWaitList: function (id, pos) {
        if (API.getWaitListPosition(id) > -1 || API.getWaitList().length < 50) {
            API.moderateMoveDJ(id, pos);
        } else {
            if ((pos = queueList.indexOf(id)) > -1) {
                queueList.splice(pos, 1);
            }
            if (API.getWaitList()[49] == null) {
                return;
            }
            user = API.getWaitList()[49].id;
            API.moderateRemoveDJ(user);
            queueList.unshift(user);
            console.log(queueList);
            API.sendChat("@" + API.getWaitList()[49].username + " you have been moved to the front of the queue for the waitlist.");
            API.moderateAddDJ(id);
            setTimeout(function () {
                API.moderateMoveDJ(id, pos);
            }, 2000);
        }
    },
    Add: function (data) {
        var msg = data.message;
        if (msg.indexOf("!join") === 0) {
            msg = msg.substr(1);
        }
        if (msg.length > 5) {
            var params = msg.substr(5), pos, id;
            pos = params.indexOf(" ", 3);
            if (params.indexOf("@") != -1) {
                var user = (pos > 0 ? params.substr(1, pos - 1) : params.substr(1));
                if (!bot.isRanked(data.uid, 2) && (user.toLowerCase() != data.un.toLowerCase())) {
                    return;
                }
                if ((id = bot.getUserID(user)) !== null) {
                    bot.AddWaitList(id);
                } else {
                    return null;
                }
                if (pos > 0) {
                    pos = parseInt(params.substr(pos + 1), 10);
                    setTimeout(function () {
                        bot.MoveWaitList(id, pos);
                    }, 2000);
                }
            }
        } else {
            bot.AddWaitList(data.uid);
        }
    },
    Remove: function (data) {
        if (data.length > 7) {
            data = data.substr(8);
            var pos;
            pos = data.indexOf(" ", 3);
            if (data.indexOf("@") != -1) {
                data = (pos > 0 ? data.substr(1,pos - 1) : data.substr(1));
                if ((data = bot.getUserID(data)) !== null) {
                    API.moderateRemoveDJ(data);
                } else {
                    return null;
                }
            } else {
                API.djLeave();
            }
        }
    },
    Move: function (data) {
        if (data.length > 4) {
            var params = data.split(' ');
            params.shift();
            var pos = parseInt(params.pop(), 10);
            var user = params.join(' ');
            user = user.replace('@', '');
            var id;
            if ((id = bot.getUserID(user)) !== null) {
                if(API.getWaitListPosition(id) == -1 && queueList.indexOf(id) == -1) {
                    bot.AddWaitList(id);
                }
            }  else {
                return null;
            }
            if (pos > 0) {
                setTimeout(function () {
                    bot.MoveWaitList(id, pos);
                }, 2000);
            }
        }
    },
    Kick: function (data) {
        if (data.length > 7) {
            var id, pos, user = data.substr(7);
            if ((pos = user.indexOf("@")) != -1) {
                user = user.substr(0, pos - 1);
            } else if (user.substr(user.length - 1) == " ") {
                user = user.substr(0, user.length - 1);
            }
            console.log("Kick -" + user);
            if (null !== (id = bot.getUserID(user))) {
                API.moderateBanUser(id, 1, API.BAN.HOUR);
            }
        } else {
            console.log("no name");
        }
    },
    Ban: function (data) {
        if (data.length > 6) {
            var id, pos, user = data.substr(6);
        }
        if ((pos = user.indexOf("@")) != -1) {
            user = user.substr(0, pos - 1);
        } else if (user.substr(user.length - 1) == " ") {
            user = user.substr(0, user.length - 1);
        }
        console.log("Ban - " + user);
        if ((id = bot.getUserID(user)) !== null) {
            API.moderateBanUser(id, 1, API.BAN.PERMA);
        } else {
            console.log("no name")
        }
    },
    Genre: function (url, newurl, author, counter) {
        console.log("Looking up " + author + " - attempt " + (counter + 1) + ": " + (newurl == '' ? url : newurl));
        if (newurl == '') newurl = url;

        var xhr = createCORSRequest('GET', 'https://gigosaurus.com/plug/wiki/' + newurl);
        if (!xhr) {
            API.sendChat('Apparently this doesn\'t work...');
            console.log("CORS isn't supported!");
        } else {
            xhr.onload = function() {
                if (xhr.responseText.length < 5) {
                    if (counter == 0) bot.Genre(url, url + '_(band)', author, counter + 1);
                    else if (counter == 1) bot.Genre(url, url.substring(0, 1).toUpperCase() + url.substr(1).toLowerCase(), author, counter + 1);
                    else if (counter == 2) bot.Genre(url, url.toProperCase(), author, counter + 1);
                    else if (counter == 3) bot.Genre(url, url.toLowerCase(), author, counter + 1);
                    else if (counter == 4) bot.Genre(url, url + '_(singer)', author, counter + 1);
                    else if (counter == 5) bot.Genre(url, url.substring(0, 1).toUpperCase() + url.substr(1).toLowerCase() + '_(band)', author, counter + 1);
                    else if (counter == 6) bot.Genre(url, url.toProperCase() + '_(band)', author, counter + 1);
                    else if (counter == 7) bot.Genre(url, url.toLowerCase() + '_(band)', author, counter + 1);
                    else if (counter == 8) bot.Genre(url, url.substring(0, 1).toUpperCase() + url.substr(1).toLowerCase() + '_(singer)', author, counter + 1);
                    else if (counter == 9) bot.Genre(url, url.toProperCase() + '_(singer)', author, counter + 1);
                    else if (counter == 10) bot.Genre(url, url.toLowerCase() + '_(singer)', author, counter + 1);
                    else {
                        API.sendChat('Can\'t find a valid page for "' + author + '"');
                        console.log("Couldnt find a valid page!");
                        return;
                    }
                }
                API.sendChat(xhr.responseText);
                console.log("Success!");
            };

            xhr.onerror = function() {
                API.sendChat("Failed to connect!");
                console.log("gigosaurus.com might be down :(");
            };
            xhr.send();
        }
    }
};

function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {

        // Check if the XMLHttpRequest object has a "withCredentials" property.
        // "withCredentials" only exists on XMLHTTPRequest2 objects.
        xhr.open(method, url, true);

    } else if (typeof XDomainRequest != "undefined") {

        // Otherwise, check if XDomainRequest.
        // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
        xhr = new XDomainRequest();
        xhr.open(method, url);

    } else {

        // Otherwise, CORS is not supported by the browser.
        xhr = null;

    }
    return xhr;
}

jQuery.getScript('https://gigosaurus.com/plug/wikipedia.js', function () {
    WIKIPEDIA.getData = function (wikipediaUrlOrPageName, callback, error) {
        var url = WIKIPEDIA._getDbpediaUrl(wikipediaUrlOrPageName);
        function onSuccess(data) {
            var out = {
                raw: data,
                dbpediaUrl: url,
                summary: null
            };
            if (data[url]) out.summary = WIKIPEDIA.extractSummary(url, data);
            else out.error = 'Failed to retrieve data. Is the URL or page name correct?';
            callback(out);
        }
        WIKIPEDIA.getRawJson(url, onSuccess, error);
    };

});

function base() {
    if ("undefined" == typeof botRunning || 0 === botRunning) {
        JoinMsg = 1;
        lockdown = 0;
        theme = "";
        API.on(API.USER_JOIN, bot.OnJoin);
        API.on(API.USER_LEAVE, bot.OnLeave);
        API.on(API.CURATE_UPDATE, bot.OnCurate);
        API.on(API.CHAT, bot.OnChat);
        API.on(API.ADVANCE, bot.OnAdvance);
        API.on(API.VOTE_UPDATE, bot.OnVoteUpdate);
        API.on(API.CHAT_COMMAND, bot.OnCommand);
        API.on(API.WAIT_LIST_UPDATE, bot.OnListUpdate);
        String.prototype.toProperCase = function () {
            return this.toLowerCase().replace(/(^|[\s _])[^\s _]/g, function (s) { return s.toUpperCase(); });
        };
        bot.AutoSkip();
        bot.initAFK();
        botRunning = 1;
        API.sendChat("House of Rock & Metal v" + nxVersion + " launched");
    } else console.log("Bot is already running");
}

function destroy() {
    API.off(API.USER_JOIN, bot.OnJoin);
    API.off(API.CHAT, bot.OnChat);
    API.off(API.ADVANCE, bot.OnAdvance);
    API.off(API.USER_LEAVE, bot.OnLeave);
    API.off(API.CURATE_UPDATE, bot.OnCurate);
    API.off(API.VOTE_UPDATE, bot.OnVoteUpdate);
    API.off(API.CHAT_COMMAND, bot.OnCommand);
    API.off(API.WAIT_LIST_UPDATE, bot.OnListUpdate);
    bot = "";
    botRunning = 0;
    API.sendChat("I ... just ...  got kille... ARGHHH");
}

base();